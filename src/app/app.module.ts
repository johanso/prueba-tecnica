import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { ProfileComponent } from './components/profile/profile.component';
import { InformationComponent } from './components/profile/information/information.component';
import { ProfessionalComponent } from './components/profile/professional/professional.component';
import { SpecialtiesComponent } from './components/profile/specialties/specialties.component';
import { AvailabilityComponent } from './components/profile/availability/availability.component';

@NgModule({
  declarations: [
    AppComponent,
    ProfileComponent,
    InformationComponent,
    ProfessionalComponent,
    SpecialtiesComponent,
    AvailabilityComponent
  ],
  imports: [
    BrowserModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
